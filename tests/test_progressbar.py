#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from pdfpug.common import Color
from pdfpug.modules import ProgressBar


class TestProgressBar:
    def test_bare_progress_bar(self):
        progress_bar = ProgressBar(percent=50)
        # fmt: off
        assert progress_bar.pug == (
            ".ui.progress\n"
            '\t.bar(style="width: 50%")'
        )
        # fmt: on

    def test_title_and_subtitle(self):
        progress_bar = ProgressBar(50, title="PdfPug", subtitle="Ease of Use")
        assert progress_bar.pug == (
            'p(style="text-align:left;margin-bottom:3px") <b>PdfPug</b>\n'
            '.ui.progress(style="margin-top:0px")\n'
            '\t.bar(style="width: 50%")\n'
            '\t.label(style="text-align:right;font-size:14px") Ease of Use'
        )

    def test_color_attribute(self):
        progress_bar = ProgressBar(50, color=Color.red)
        # fmt: off
        assert progress_bar.pug == (
            ".ui.red.progress\n"
            '\t.bar(style="width: 50%")'
        )
        # fmt: on
