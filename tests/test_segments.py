#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from pdfpug.common import SegmentType
from pdfpug.modules import Segment, Segments


class TestSegments:
    def test_basic_group_of_segments(self):
        segments = Segments([Segment("Segment 1"), Segment("Segment 2")])
        assert segments.pug == (
            ".ui.segments\n"
            "\t.ui.segment\n"
            '\t\tp(style="text-align:left") Segment 1\n'
            "\t.ui.segment\n"
            '\t\tp(style="text-align:left") Segment 2'
        )

    def test_piled_group_of_segments(self):
        segments = Segments(
            [Segment("Segment 1"), Segment("Segment 2")],
            segments_type=SegmentType.piled,
        )
        assert segments.pug == (
            ".ui.piled.segments\n"
            "\t.ui.segment\n"
            '\t\tp(style="text-align:left") Segment 1\n'
            "\t.ui.segment\n"
            '\t\tp(style="text-align:left") Segment 2'
        )
