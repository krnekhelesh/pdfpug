#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import pytest

from pdfpug.common import Color, SegmentSpacing, SegmentEmphasis, SegmentType, Alignment
from pdfpug.modules import Header, Segment


class TestSegment:
    def test_str_initialisation_segment(self):
        segment = Segment("PdfPug Segment Body")
        assert segment.pug == (
            ".ui.segment\n" '\tp(style="text-align:left") PdfPug Segment Body'
        )

    def test_list_initialisation_segment(self):
        segment = Segment([Header("Segment Header"), "PdfPug Segment Body"])
        assert segment.pug == (
            ".ui.segment\n"
            '\th1(class="ui center aligned header", id="Segment-Header") Segment Header\n'
            '\tp(style="text-align:left") PdfPug Segment Body'
        )

    def test_invalid_initialisation(self):
        with pytest.raises(TypeError):
            Segment()

    @pytest.mark.parametrize(
        "segment_type,expected_pug",
        [
            (
                SegmentType.stacked,
                '.ui.stacked.segment\n\tp(style="text-align:left") PdfPug Segment Body',
            ),
            (
                SegmentType.piled,
                '.ui.piled.segment\n\tp(style="text-align:left") PdfPug Segment Body',
            ),
            (
                SegmentType.circular,
                '.ui.circular.segment\n\tp(style="text-align:left") PdfPug Segment Body',
            ),
            (
                SegmentType.basic,
                '.ui.basic.segment\n\tp(style="text-align:left") PdfPug Segment Body',
            ),
        ],
    )
    def test_segment_types(self, segment_type, expected_pug):
        segment = Segment("PdfPug Segment Body", segment_type=segment_type)
        assert segment.pug == expected_pug

    @pytest.mark.parametrize(
        "emphasis,expected_pug",
        [
            (
                SegmentEmphasis.secondary,
                '.ui.secondary.segment\n\tp(style="text-align:left") PdfPug Segment Body',
            ),
            (
                SegmentEmphasis.tertiary,
                '.ui.tertiary.segment\n\tp(style="text-align:left") PdfPug Segment Body',
            ),
        ],
    )
    def test_segment_emphasis(self, emphasis, expected_pug):
        segment = Segment("PdfPug Segment Body", emphasis=emphasis)
        assert segment.pug == expected_pug

    @pytest.mark.parametrize(
        "color,expected_pug",
        [
            (
                Color.red,
                '.ui.red.segment\n\tp(style="text-align:left") PdfPug Segment Body',
            ),
            (
                Color.green,
                '.ui.green.segment\n\tp(style="text-align:left") PdfPug Segment Body',
            ),
        ],
    )
    def test_segment_color(self, color, expected_pug):
        segment = Segment("PdfPug Segment Body", color=color)
        assert segment.pug == expected_pug

    def test_str_segment_alignment(self):
        segment = Segment("PdfPug Segment Body", alignment=Alignment.center)
        assert segment.pug == (
            ".ui.center.aligned.segment\n"
            '\tp(style="text-align:left") PdfPug Segment Body'
        )

    def test_segment_alignment(self):
        segment = Segment([Header("Segment Header")], alignment=Alignment.right)
        assert segment.pug == (
            ".ui.right.aligned.segment\n"
            '\th1(class="ui right aligned header", id="Segment-Header") Segment Header'
        )

    @pytest.mark.parametrize(
        "spacing,expected_pug",
        [
            (
                SegmentSpacing.padded,
                '.ui.padded.segment\n\tp(style="text-align:left") PdfPug Segment Body',
            ),
            (
                SegmentSpacing.compact,
                '.ui.compact.segment\n\tp(style="text-align:left") PdfPug Segment Body',
            ),
        ],
    )
    def test_segment_spacing(self, spacing, expected_pug):
        segment = Segment("PdfPug Segment Body", spacing=spacing)
        assert segment.pug == expected_pug
