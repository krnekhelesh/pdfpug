#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import pytest

from pdfpug.common import Size, HeaderTier, Alignment, HeaderStyle, Color
from pdfpug.modules import Header


class TestHeader:
    def test_default_header(self):
        header = Header("PdfPug Header")
        assert (
            header.pug
            == 'h1(class="ui center aligned header", id="PdfPug-Header") PdfPug Header'
        )

    def test_header_repr(self):
        header = Header("PdfPug Header")
        assert repr(header) == "Header(PdfPug Header)"

    def test_sub_header(self):
        header = Header("PdfPug Header", sub_header="SubHeader")
        assert header.pug == (
            'h1(class="ui center aligned header", id="PdfPug-Header") PdfPug Header\n'
            "\t.sub.header SubHeader"
        )

    def test_header_inception(self):
        header = Header("PdfPug Header")
        header.depth = 1
        assert (
            header.pug
            == '\th1(class="ui center aligned header", id="PdfPug-Header") PdfPug Header'
        )

    def test_invalid_initialisation(self):
        with pytest.raises(TypeError):
            Header()

        with pytest.raises(ValueError):
            Header("PdfPug Header", tier=HeaderTier.h1, size=Size.small).pug

    @pytest.mark.parametrize(
        "tier,expected_pug",
        [
            (
                HeaderTier.h1,
                'h1(class="ui center aligned header", id="PdfPug-Header") PdfPug Header',
            ),
            (
                HeaderTier.h2,
                'h2(class="ui center aligned header", id="PdfPug-Header") PdfPug Header',
            ),
            (
                HeaderTier.h3,
                'h3(class="ui center aligned header", id="PdfPug-Header") PdfPug Header',
            ),
            (
                HeaderTier.h4,
                'h4(class="ui center aligned header", id="PdfPug-Header") PdfPug Header',
            ),
            (
                HeaderTier.h5,
                'h5(class="ui center aligned header", id="PdfPug-Header") PdfPug Header',
            ),
        ],
    )
    def test_header_tiers(self, tier, expected_pug):
        header = Header("PdfPug Header", tier=tier)
        assert header.pug == expected_pug

    @pytest.mark.parametrize(
        "alignment,expected_pug",
        [
            (
                Alignment.left,
                'h1(class="ui left aligned header", id="PdfPug-Header") PdfPug Header',
            ),
            (
                Alignment.right,
                'h1(class="ui right aligned header", id="PdfPug-Header") PdfPug Header',
            ),
            (
                Alignment.center,
                'h1(class="ui center aligned header", id="PdfPug-Header") PdfPug Header',
            ),
            (
                Alignment.justified,
                'h1(class="ui justified header", id="PdfPug-Header") PdfPug Header',
            ),
        ],
    )
    def test_header_alignments(self, alignment, expected_pug):
        header = Header("PdfPug Header", alignment=alignment)
        assert header.pug == expected_pug

    def test_header_size(self):
        header = Header("PdfPug Header", tier=None, size=Size.small)
        assert (
            header.pug
            == '.ui.small.center.aligned.header(id="PdfPug-Header") PdfPug Header'
        )

    @pytest.mark.parametrize(
        "style,expected_pug",
        [
            (
                HeaderStyle.block,
                'h1(class="ui center aligned block header", id="PdfPug-Header") PdfPug Header',
            ),
            (
                HeaderStyle.dividing,
                'h1(class="ui center aligned dividing header", id="PdfPug-Header") PdfPug Header',
            ),
        ],
    )
    def test_header_style(self, style, expected_pug):
        header = Header("PdfPug Header", style=style)
        assert header.pug == expected_pug

    @pytest.mark.parametrize(
        "color,expected_pug",
        [
            (
                Color.red,
                'h1(class="ui center aligned red header", id="PdfPug-Header") PdfPug Header',
            ),
            (
                Color.blue,
                'h1(class="ui center aligned blue header", id="PdfPug-Header") PdfPug Header',
            ),
            (
                Color.green,
                'h1(class="ui center aligned green header", id="PdfPug-Header") PdfPug Header',
            ),
        ],
    )
    def test_header_color(self, color, expected_pug):
        header = Header("PdfPug Header", color=color)
        assert header.pug == expected_pug
