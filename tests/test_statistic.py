#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from pdfpug.common import Size
from pdfpug.modules import Statistic


class TestStatistic:
    def test_basic_statistic(self):
        stat = Statistic("PdfPug", 1500)
        # fmt: off
        assert stat.pug == (
            ".ui.statistic"
            "\n\t.value 1500"
            "\n\t.label PdfPug"
        )
        # fmt: on

    def test_different_value_type_statistic(self):
        stat = Statistic("PdfPug", "Thousand Five Hundred")
        # fmt: off
        assert stat.pug == (
            ".ui.statistic"
            "\n\t.text.value Thousand Five Hundred"
            "\n\t.label PdfPug"
        )
        # fmt: on

    def test_statistic_customisation(self):
        stat = Statistic("PdfPug", 1500, size=Size.small)
        # fmt: off
        assert stat.pug == (
            ".ui.small.statistic"
            "\n\t.value 1500"
            "\n\t.label PdfPug"
        )
        # fmt: on
