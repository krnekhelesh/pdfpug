#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from pdfpug.common import bold, italic, underline, superscript, subscript, url, strike


class TestCommonHelpers:
    def test_bold_formatting(self):
        assert bold("PdfPug") == "<b>PdfPug</b>"

    def test_italic_formatting(self):
        assert italic("PdfPug") == "<i>PdfPug</i>"

    def test_underline_formatting(self):
        assert underline("PdfPug") == "<u>PdfPug</u>"

    def test_superscript_formatting(self):
        assert superscript("PdfPug") == "<sup>PdfPug</sup>"

    def test_subscript_formatting(self):
        assert subscript("PdfPug") == "<sub>PdfPug</sub>"

    def test_strike_formatting(self):
        assert strike("PdfPug") == "<strike>PdfPug</strike>"

    def test_url(self):
        assert url("https://www.google.com", "Visible String") == (
            '<a href="https://www.google.com">Visible String</a>'
        )

    def test_only_url(self):
        assert url("https://www.google.com") == (
            '<a href="https://www.google.com">https://www.google.com</a>'
        )
