#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from pdfpug.modules import Table, Cell, Segment, Row


class TestTable:
    def test_basic_table(self):
        table = Table(
            header=["Fruit", "Stock Level"], data=[["Apple", "Low"], ["Orange", "High"]]
        )

        assert table.pug == (
            'table(class="ui padded celled table")\n'
            "\tthead\n"
            "\t\ttr\n"
            "\t\t\tth Fruit\n"
            "\t\t\tth Stock Level\n"
            "\ttbody\n"
            "\t\ttr\n"
            "\t\t\ttd Apple\n"
            "\t\t\ttd Low\n"
            "\t\ttr\n"
            "\t\t\ttd Orange\n"
            "\t\t\ttd High\n"
        )

    def test_headerless_table(self):
        table = Table(data=[["Apple", "Low"], ["Orange", "High"]])

        assert table.pug == (
            'table(class="ui padded celled table")\n'
            "\ttbody\n"
            "\t\ttr\n"
            "\t\t\ttd Apple\n"
            "\t\t\ttd Low\n"
            "\t\ttr\n"
            "\t\t\ttd Orange\n"
            "\t\t\ttd High\n"
        )

    def test_advanced_table(self):
        table = Table(
            header=["Fruit", "Stock Level"],
            data=[["Apple", Cell("Low", row_span=2)], ["Orange"]],
        )

        assert table.pug == (
            'table(class="ui padded celled table")\n'
            "\tthead\n"
            "\t\ttr\n"
            "\t\t\tth Fruit\n"
            "\t\t\tth Stock Level\n"
            "\ttbody\n"
            "\t\ttr\n"
            "\t\t\ttd Apple\n"
            '\t\t\ttd(rowspan="2") Low\n'
            "\t\ttr\n"
            "\t\t\ttd Orange\n"
        )

    def test_table_inception(self):
        table = Table(
            header=Row(["Fruit", "Stock Level"]),
            data=[["Apple", "Low"], ["Orange", "High"]],
        )
        segment = Segment([table])
        assert segment.pug == (
            ".ui.segment\n"
            '\ttable(class="ui padded celled table")\n'
            "\t\tthead\n"
            "\t\t\ttr\n"
            "\t\t\t\tth Fruit\n"
            "\t\t\t\tth Stock Level\n"
            "\t\ttbody\n"
            "\t\t\ttr\n"
            "\t\t\t\ttd Apple\n"
            "\t\t\t\ttd Low\n"
            "\t\t\ttr\n"
            "\t\t\t\ttd Orange\n"
            "\t\t\t\ttd High\n"
        )

    def test_advanced_table_inception(self):
        table = Table(
            header=["Fruit", "Stock Level"],
            data=[["Apple", Cell("Low", row_span=2)], Row(["Orange"])],
        )

        segment = Segment([table])

        assert segment.pug == (
            ".ui.segment\n"
            '\ttable(class="ui padded celled table")\n'
            "\t\tthead\n"
            "\t\t\ttr\n"
            "\t\t\t\tth Fruit\n"
            "\t\t\t\tth Stock Level\n"
            "\t\ttbody\n"
            "\t\t\ttr\n"
            "\t\t\t\ttd Apple\n"
            '\t\t\t\ttd(rowspan="2") Low\n'
            "\t\t\ttr\n"
            "\t\t\t\ttd Orange\n"
        )
