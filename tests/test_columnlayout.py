#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import pytest

from pdfpug.layouts import Column
from pdfpug.modules import Header


class TestColumnLayout:
    def test_column_init(self):
        column = Column()
        assert column.pug == ".column"

    def test_column_content(self):
        column = Column()
        column.add_element(Header("PdfPug"))
        # fmt: off
        assert column.pug == (
            ".column\n"
            '\th1(class="ui center aligned header", id="PdfPug") PdfPug'
        )
        # fmt: on

    def test_invalid_column_width(self):
        column = Column(width=15)
        with pytest.raises(ValueError):
            column.pug

    def test_invalid_element(self):
        column = Column()
        with pytest.raises(TypeError):
            column.add_element(["PdfPug"])
