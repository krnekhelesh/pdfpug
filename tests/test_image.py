#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import pytest

from pdfpug.common import Size, ImageLayout, ImageStyle
from pdfpug.modules import Image


class TestImage:
    def test_default_image(self):
        image = Image("/test/path/image.png")
        assert image.pug == 'img(class="ui image" src="file:///test/path/image.png")'

    def test_image_size(self):
        image = Image("/test/path/image.png", size=Size.small)
        assert (
            image.pug == 'img(class="ui small image" src="file:///test/path/image.png")'
        )

    @pytest.mark.parametrize(
        "style,expected_pug",
        [
            (
                ImageStyle.circular,
                'img(class="ui circular image" src="file:///test/path/image.png")',
            ),
            (
                ImageStyle.avatar,
                'img(class="ui avatar image" src="file:///test/path/image.png")',
            ),
            (
                ImageStyle.rounded,
                'img(class="ui rounded image" src="file:///test/path/image.png")',
            ),
        ],
    )
    def test_image_style(self, style, expected_pug):
        image = Image("/test/path/image.png", style=style)
        assert image.pug == expected_pug

    @pytest.mark.parametrize(
        "layout,expected_pug",
        [
            (
                ImageLayout.left_float,
                'img(class="ui left float image" src="file:///test/path/image.png")',
            ),
            (
                ImageLayout.right_float,
                'img(class="ui right float image" src="file:///test/path/image.png")',
            ),
            (
                ImageLayout.centered,
                'img(class="ui centered image" src="file:///test/path/image.png")',
            ),
        ],
    )
    def test_image_layout(self, layout, expected_pug):
        image = Image("/test/path/image.png", layout=layout)
        assert image.pug == expected_pug
