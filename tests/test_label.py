#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from pdfpug.modules import Label
from pdfpug.common import Color, LabelType


class TestLabel:
    def test_basic_label(self):
        label = Label(text="PdfPug")
        assert label.pug == ".ui.label PdfPug"

    def test_customised_label(self):
        label = Label(text="PdfPug", color=Color.red, label_type=LabelType.tag)
        assert label.pug == ".ui.red.tag.label PdfPug"

    def test_label_subtext(self):
        label = Label(text="PdfPug", subtext="PDF Generator")
        # fmt: off
        assert label.pug == (
            ".ui.label PdfPug"
            "\n\t.detail PDF Generator"
        )
        # fmt: on

    def test_empty_circular_label(self):
        label = Label(label_type=LabelType.circular)
        assert label.pug == ".ui.empty.circular.label"
