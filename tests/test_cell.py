#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import pytest

from pdfpug.common import TableRowType, Alignment, State
from pdfpug.modules import Cell, Header


class TestCell:
    def test_default_cell(self):
        cell = Cell("PdfPug")
        assert cell.pug == "\t\t\ttd PdfPug\n"

    def test_invalid_initialisation(self):
        cell = Cell(["PdfPug"])
        with pytest.raises(ValueError):
            cell.pug

    def test_cell_inception(self):
        cell = Cell(Header("PdfPug"))
        assert cell.pug == (
            '\t\t\ttd \n\t\t\t\th1(class="ui center aligned header", id="PdfPug") '
            "PdfPug\n"
        )

    def test_header_cell(self):
        cell = Cell("PdfPug", cell_type=TableRowType.header)
        assert cell.pug == "\t\t\tth PdfPug\n"

    def test_rowspan_cell(self):
        cell = Cell("PdfPug", row_span=2)
        assert cell.pug == '\t\t\ttd(rowspan="2") PdfPug\n'

    def test_columnspan_cell(self):
        cell = Cell("PdfPug", column_span=2)
        assert cell.pug == '\t\t\ttd(columnspan="2") PdfPug\n'

    def test_cell_width(self):
        cell = Cell("PdfPug", width=5, cell_type=TableRowType.header)
        assert cell.pug == '\t\t\tth(class="five wide") PdfPug\n'

    def test_cell_content_alignment(self):
        cell = Cell("PdfPug", alignment=Alignment.right)
        assert cell.pug == '\t\t\ttd(class="right aligned") PdfPug\n'

    @pytest.mark.parametrize(
        "state,expected_pug",
        [
            (State.active, '\t\t\ttd(class="active") PdfPug\n'),
            (State.negative, '\t\t\ttd(class="negative") PdfPug\n'),
            (State.error, '\t\t\ttd(class="error") PdfPug\n'),
            (State.positive, '\t\t\ttd(class="positive") PdfPug\n'),
            (State.disabled, '\t\t\ttd(class="disabled") PdfPug\n'),
            (State.warning, '\t\t\ttd(class="warning") PdfPug\n'),
        ],
    )
    def test_cell_state(self, state, expected_pug):
        cell = Cell("PdfPug", state=state)
        assert cell.pug == expected_pug
