#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from pdfpug.modules import TableOfContents, Header, Paragraph
from pdfpug.common import HeaderTier


class TestTableOfContents:
    def test_basic_toc(self):
        elements = [Header("PdfPug"), Header("H2", tier=HeaderTier.h2)]
        toc = TableOfContents()
        toc._elements = elements
        assert toc.pug == (
            'h1(class="ui left aligned header", id="Table-of-Contents") '
            "Table of Contents\n"
            '.ui.ordered.vertical.small.list(id="Table-of-Contents-List")\n'
            '\t.item <a href="#PdfPug">PdfPug</a>\n'
            "\t\t.ui.vertical.small.list\n"
            '\t\t\t.item <a href="#H2">H2</a>\n'
        )

    def test_missing_h1_header_toc(self):
        elements = [Header("H2", tier=HeaderTier.h2)]
        toc = TableOfContents()
        toc._elements = elements
        assert toc.pug == (
            'h1(class="ui left aligned header", id="Table-of-Contents") '
            "Table of Contents\n"
            '.ui.ordered.vertical.small.list(id="Table-of-Contents-List")\n'
            "\t.item H1 Header\n"
            "\t\t.ui.vertical.small.list\n"
            '\t\t\t.item <a href="#H2">H2</a>\n'
        )

    def test_filter_other_elements_toc(self):
        elements = [Header("PdfPug"), Paragraph("Test")]
        toc = TableOfContents()
        toc._elements = elements
        assert toc.pug == (
            'h1(class="ui left aligned header", id="Table-of-Contents") '
            "Table of Contents\n"
            '.ui.ordered.vertical.small.list(id="Table-of-Contents-List")\n'
            '\t.item <a href="#PdfPug">PdfPug</a>\n'
        )

    def test_filter_h3_headers_toc(self):
        elements = [Header("PdfPug"), Header("H3", tier=HeaderTier.h3)]
        toc = TableOfContents()
        toc._elements = elements
        assert toc.pug == (
            'h1(class="ui left aligned header", id="Table-of-Contents") '
            "Table of Contents\n"
            '.ui.ordered.vertical.small.list(id="Table-of-Contents-List")\n'
            '\t.item <a href="#PdfPug">PdfPug</a>\n'
        )
