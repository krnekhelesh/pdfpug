#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import pytest

from pdfpug.modules import BaseList, OrderedList, UnorderedList


class TestList:
    def test_basic_list(self):
        base_list = BaseList(["Item 1", 2, 2.3])
        # fmt: off
        assert base_list.pug == (
            ".ui.vertical.small.list\n"
            "\t.item Item 1\n"
            "\t.item 2\n"
            "\t.item 2.3\n"
        )
        # fmt: on

    def test_nested_list(self):
        base_list = BaseList([{"Item 1": ["Item 1.1", "Item 1.2"]}])
        assert base_list.pug == (
            ".ui.vertical.small.list\n"
            "\t.item Item 1\n"
            "\t\t.ui.vertical.small.list\n"
            "\t\t\t.item Item 1.1\n"
            "\t\t\t.item Item 1.2\n"
        )

    def test_invalid_nested_list(self):
        base_list = BaseList([{"Item 1": {"Item 1.1", "Item 1.2"}}])
        with pytest.raises(TypeError):
            base_list.pug

    def test_another_invalid_nested_list(self):
        base_list = BaseList(["Item 1", ["Item 1.1", "Item 1.2"]])
        with pytest.raises(TypeError):
            base_list.pug

    def test_invalid_list(self):
        base_list = BaseList("Item 1")
        with pytest.raises(TypeError):
            base_list.pug


class TestOrderedList:
    def test_basic_ordered_list(self):
        ordered_list = OrderedList(["Item 1", "Item 2"])
        assert ordered_list.pug == (
            ".ui.ordered.vertical.small.list\n" "\t.item Item 1\n" "\t.item Item 2\n"
        )


class TestUnorderedList:
    def test_basic_unordered_list(self):
        unordered_list = UnorderedList(["Item 1", "Item 2"])
        assert unordered_list.pug == (
            ".ui.bulleted.vertical.small.list\n" "\t.item Item 1\n" "\t.item Item 2\n"
        )
