#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import pytest

from pdfpug.common import Color
from pdfpug.modules import MessageBox


class TestMessageBox:
    def test_simple_messagebox(self):
        message_box = MessageBox(header="Title", body="Body")
        # fmt: off
        assert message_box.pug == (
            ".ui.message\n"
            "\t.header Title\n"
            '\tp(style="text-align:left") Body'
        )
        # fmt: on

    def test_invalid_body(self):
        message_box = MessageBox(body={"test"})
        with pytest.raises(TypeError):
            message_box.pug

    def test_list_messagebox(self):
        message_box = MessageBox(header="Title", body=["Item 1", "Item 2"])
        assert message_box.pug == (
            ".ui.message\n"
            "\t.header Title\n"
            '\tul(class="list")\n'
            "\t\tli Item 1\n"
            "\t\tli Item 2"
        )

    def test_headerless_messagebox(self):
        message_box = MessageBox("Body")
        # fmt: off
        assert message_box.pug == (
            ".ui.message\n"
            '\tp(style="text-align:left") Body'
        )
        # fmt: on

    def test_colored_messagebox(self):
        message_box = MessageBox("Body", color=Color.red)
        # fmt: off
        assert message_box.pug == (
            ".ui.red.message\n"
            '\tp(style="text-align:left") Body'
        )
        # fmt: on
