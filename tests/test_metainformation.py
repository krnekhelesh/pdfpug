#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from pdfpug.modules.metainformation import MetaInformation


class TestMetaInformation:
    def test_no_args_call(self):
        meta = MetaInformation(version="0.5.0")
        assert meta.pug == '\nmeta(name="generator" content="PdfPug 0.5.0")'

    def test_all_args(self):
        meta = MetaInformation(
            version="0.5.0",
            authors=["Nekhelesh Ramananthan"],
            keywords=["pdf", "pug"],
            title="PdfPug",
            description="Create Rich Beautiful PDFs",
        )
        assert meta.pug == (
            f"\ntitle PdfPug"
            f'\nmeta(name="author" content="Nekhelesh Ramananthan")'
            f'\nmeta(name="keywords" content="pdf,pug")'
            f'\nmeta(name="description" content="Create Rich Beautiful PDFs")'
            f'\nmeta(name="generator" content="PdfPug 0.5.0")'
        )
