#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from pdfpug.common import Size
from pdfpug.modules import Image, Images


class TestImages:
    def test_basic_group_of_images(self):
        images = Images([Image("/test/path/image.png"), Image("/test/path/image.png")])
        assert images.pug == (
            ".ui.images\n"
            '\timg(class="ui image" src="file:///test/path/image.png")\n'
            '\timg(class="ui image" src="file:///test/path/image.png")\n'
        )

    def test_large_group_of_images(self):
        images = Images(
            [Image("/test/path/image.png"), Image("/test/path/image.png")],
            size=Size.large,
        )

        assert images.pug == (
            ".ui.large.images\n"
            '\timg(class="ui image" src="file:///test/path/image.png")\n'
            '\timg(class="ui image" src="file:///test/path/image.png")\n'
        )
