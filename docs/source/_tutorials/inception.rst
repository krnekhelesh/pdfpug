Inception
=========

This tutorial is one you would enjoy if you read the output PDF instead
of boring paragraphs of text here. It all starts with one of my favorite
movie Inception.

The final output would look something like the screenshot below.

.. figure:: ../_images/inception.png
    :height: 500

The source code and the output PDF file can be downloaded here. If you notice
any discrepancies, do report a bug.
:download:`Source Code <../_samples/inception/inception.py>`,
:download:`Output PDF <../_samples/inception/inception.pdf>`,
:download:`Picture 1 <../_samples/inception/caprio.jpg>`
:download:`Picture 2 <../_samples/inception/joseph.jpg>`
:download:`Picture 3 <../_samples/inception/ellen.jpg>`
:download:`Picture 4 <../_samples/inception/tom.jpg>`
