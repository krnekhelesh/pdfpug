PdfReport
=========

.. autoclass:: pdfpug.PdfReport
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.PageSize
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.PageOrientation
    :members:
    :member-order: bysource
