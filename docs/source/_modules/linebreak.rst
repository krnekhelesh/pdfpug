LineBreak
=========

.. autoclass:: pdfpug.modules.LineBreak
    :members:
    :member-order: bysource
