Progress Bar
============

.. autoclass:: pdfpug.modules.ProgressBar
    :members:
    :member-order: bysource
