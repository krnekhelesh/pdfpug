Header
======

.. autoclass:: pdfpug.modules.Header
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.HeaderTier
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.HeaderStyle
    :members:
    :member-order: bysource
