Message Box
===========

.. autoclass:: pdfpug.modules.MessageBox
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.MessageState
    :members:
    :member-order: bysource
