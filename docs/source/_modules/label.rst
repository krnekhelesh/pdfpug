Label
=====

.. autoclass:: pdfpug.modules.Label
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.LabelType
    :members:
    :member-order: bysource
