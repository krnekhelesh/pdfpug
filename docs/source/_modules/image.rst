Image
=====

.. autoclass:: pdfpug.modules.Image
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.modules.Images
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.ImageStyle
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.ImageLayout
    :members:
    :member-order: bysource
