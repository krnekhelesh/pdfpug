Statistic
=========

.. autoclass:: pdfpug.modules.Statistic
    :members:
    :member-order: bysource
