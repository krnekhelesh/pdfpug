PageBreak
=========

.. autoclass:: pdfpug.modules.PageBreak
    :members:
    :member-order: bysource
