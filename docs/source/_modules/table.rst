Table
=====

.. autoclass:: pdfpug.modules.Table
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.modules.Row
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.modules.Cell
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.TableType
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.TableColumnWidth
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.TableSpacing
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.TableRowStyle
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.TableRowType
    :members:
    :member-order: bysource
