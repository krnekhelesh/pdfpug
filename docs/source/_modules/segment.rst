Segment
=======

.. autoclass:: pdfpug.modules.Segment
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.modules.Segments
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.SegmentType
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.SegmentSpacing
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.SegmentEmphasis
    :members:
    :member-order: bysource
