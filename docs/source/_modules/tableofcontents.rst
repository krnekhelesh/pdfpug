Table of Contents
=================

.. autoclass:: pdfpug.modules.TableOfContents
    :members:
    :member-order: bysource
