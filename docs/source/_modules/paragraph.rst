Paragraph
=========

.. autoclass:: pdfpug.modules.Paragraph
    :members:
    :member-order: by-source

.. autoclass:: pdfpug.common.ParagraphAlignment
    :members:
    :member-order: by-source
