#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os

from pdfpug import PdfReport
from pdfpug.common import (
    HeaderTier,
    HeaderStyle,
    Alignment,
    Size,
    ImageStyle,
    ImageLayout,
    ParagraphAlignment,
    SegmentType,
    SegmentSpacing,
    TableSpacing,
    TableType,
    Color,
)
from pdfpug.layouts import Column, Grid
from pdfpug.modules import (
    Header,
    Image,
    Paragraph,
    LineBreak,
    Table,
    Cell,
    Segment,
    ProgressBar,
    UnorderedList,
)

profile_pic = Image(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), "musk.jpeg"),
    style=ImageStyle.circular,
    size=Size.small,
    layout=ImageLayout.centered,
)

info_header = Header(
    "Info", tier=HeaderTier.h3, style=HeaderStyle.dividing, alignment=Alignment.left
)

email = Header(
    "Email",
    sub_header="elonmusk@teslamotors.com",
    alignment=Alignment.left,
    tier=HeaderTier.h5,
)

phone = Header(
    "Phone", sub_header="650-681-5000", alignment=Alignment.left, tier=HeaderTier.h5
)

address = Header(
    "Address",
    sub_header="Los Angeles, USA",
    alignment=Alignment.left,
    tier=HeaderTier.h5,
)

skills_header = Header(
    "Skills and Competences",
    tier=HeaderTier.h3,
    style=HeaderStyle.dividing,
    alignment=Alignment.left,
)

goal = ProgressBar(100, title="Goal Oriented", size=Size.small, color=Color.orange)
micromanaging = ProgressBar(
    70, title="Micromanaging", size=Size.small, color=Color.orange
)
future = ProgressBar(100, title="Future Focused", size=Size.small, color=Color.orange)
critical_thinking = ProgressBar(
    80, title="Critical Thinking", size=Size.small, color=Color.orange
)
resiliency = ProgressBar(100, title="Resiliency", size=Size.small, color=Color.orange)
time = ProgressBar(100, title="Time Management", size=Size.small, color=Color.orange)
leadership = ProgressBar(95, title="Leadership", size=Size.small, color=Color.orange)
first = ProgressBar(100, title="First Principles", size=Size.small, color=Color.orange)

# Create first page left column and add contents to it
first_page_left_column = Column(width=4)
first_page_left_column.add_element(profile_pic)
first_page_left_column.add_element(info_header)
first_page_left_column.add_element(email)
first_page_left_column.add_element(phone)
first_page_left_column.add_element(address)
first_page_left_column.add_element(skills_header)
first_page_left_column.add_element(first)
first_page_left_column.add_element(goal)
first_page_left_column.add_element(micromanaging)
first_page_left_column.add_element(future)
first_page_left_column.add_element(critical_thinking)
first_page_left_column.add_element(resiliency)
first_page_left_column.add_element(leadership)
first_page_left_column.add_element(time)

name_header = Header(
    "Elon Musk", sub_header="CEO Tesla, SpaceX, PayPal", size=Size.huge, tier=None
)

summary = Paragraph(
    "Aiming to reduce global warming through sustainable energy production and "
    'consumption, and reducing the "risk of human extinction" by '
    '"making life multi-planetary" and setting up a human colony on Mars.',
    alignment=ParagraphAlignment.center,
)

work_header = Header(
    "Work Experience",
    tier=HeaderTier.h3,
    style=HeaderStyle.dividing,
    alignment=Alignment.left,
)

work_exp = Table(
    data=[
        [
            "2006 - Present",
            Cell(
                Segment(
                    [
                        Header(
                            "Chairman",
                            sub_header="Solar City",
                            alignment=Alignment.left,
                            tier=HeaderTier.h4,
                        ),
                        Paragraph(
                            "Created a collaboration between SolarCity and Tesla to "
                            "use electric vehicle batteries to smooth the impact of "
                            "rooftop solar on the power grid. Provided the initial "
                            "concept and financial capital."
                        ),
                    ],
                    segment_type=SegmentType.basic,
                    spacing=SegmentSpacing.compact,
                )
            ),
        ],
        [
            "2004 - Present",
            Cell(
                Segment(
                    [
                        Header(
                            "CEO and Product Architect",
                            sub_header="Tesla Motors",
                            alignment=Alignment.left,
                            tier=HeaderTier.h4,
                        ),
                        Paragraph(
                            "Currently oversee the company's product strategy -- "
                            "including the design, engineering, and manufacturing of "
                            "more and more affordable eletric vehicles for mainstream "
                            "consumers."
                        ),
                    ],
                    segment_type=SegmentType.basic,
                    spacing=SegmentSpacing.compact,
                )
            ),
        ],
        [
            "2002 - Present",
            Cell(
                Segment(
                    [
                        Header(
                            "CEO and CTO",
                            sub_header="SpaceX",
                            alignment=Alignment.left,
                            tier=HeaderTier.h4,
                        ),
                        Paragraph(
                            "Plans to reduce space transportation costs to enable"
                            "people to colonize Mars. Oversee the development of "
                            "rockets and spacecraft for missions to Earth orbit and "
                            "ultimately to other planets. Developed the Falcon 9 "
                            "spacecraft which replaced the space shuttle when it "
                            "retired in 2011."
                        ),
                    ],
                    segment_type=SegmentType.basic,
                    spacing=SegmentSpacing.compact,
                )
            ),
        ],
        [
            "1999 - 2002",
            Cell(
                Segment(
                    [
                        Header(
                            "CEO",
                            sub_header="X.com and Paypal",
                            alignment=Alignment.left,
                            tier=HeaderTier.h4,
                        )
                    ],
                    segment_type=SegmentType.basic,
                    spacing=SegmentSpacing.compact,
                )
            ),
        ],
        [
            "1995 - 1999",
            Cell(
                Segment(
                    [
                        Header(
                            "Co-founder",
                            sub_header="Zip2",
                            alignment=Alignment.left,
                            tier=HeaderTier.h4,
                        )
                    ],
                    segment_type=SegmentType.basic,
                    spacing=SegmentSpacing.compact,
                )
            ),
        ],
    ],
    spacing=TableSpacing.compact,
    table_type=TableType.bare,
)

# Create first page right column and add contents to it
first_page_right_column = Column(width=10)
first_page_right_column.add_element(name_header)
first_page_right_column.add_element(LineBreak())
first_page_right_column.add_element(summary)
first_page_right_column.add_element(work_header)
first_page_right_column.add_element(work_exp)

first_page_grid = Grid()
first_page_grid.add_layout(first_page_left_column)
first_page_grid.add_layout(first_page_right_column)

language_header = Header(
    "Languages",
    style=HeaderStyle.dividing,
    tier=HeaderTier.h3,
    alignment=Alignment.left,
)

languages = UnorderedList(["English", "Afrikaans"])

interest_header = Header(
    "Interests",
    style=HeaderStyle.dividing,
    tier=HeaderTier.h3,
    alignment=Alignment.left,
)

interests = UnorderedList(
    [
        "Physics",
        "Sustainability",
        "Philanthropy",
        "Extraterrestrial life",
        "Space Engineering",
        "Reading",
        "Video Games",
        "Alternative energy sources",
    ]
)

# Create second page left column and add contents to it
second_page_left_column = Column(width=4)
second_page_left_column.add_element(language_header)
second_page_left_column.add_element(languages)
second_page_left_column.add_element(interest_header)
second_page_left_column.add_element(interests)

education_header = Header(
    "Education",
    style=HeaderStyle.dividing,
    tier=HeaderTier.h3,
    alignment=Alignment.left,
)

educations = Table(
    data=[
        [
            "1992 - 1995",
            Cell(
                Segment(
                    [
                        Header(
                            "Bachelor of Science in Economics",
                            sub_header="Wharton School of the University of Pennsylvania",
                            alignment=Alignment.left,
                            tier=HeaderTier.h4,
                        )
                    ],
                    segment_type=SegmentType.basic,
                    spacing=SegmentSpacing.compact,
                )
            ),
        ],
        [
            "1992 - 1995",
            Cell(
                Segment(
                    [
                        Header(
                            "Bachelor of Science in Physics",
                            sub_header="Penn's College of Arts and Science",
                            alignment=Alignment.left,
                            tier=HeaderTier.h4,
                        )
                    ],
                    segment_type=SegmentType.basic,
                    spacing=SegmentSpacing.compact,
                )
            ),
        ],
    ],
    spacing=TableSpacing.compact,
    table_type=TableType.bare,
)

achievement_header = Header(
    "Achievements & Certificates",
    style=HeaderStyle.dividing,
    tier=HeaderTier.h3,
    alignment=Alignment.left,
)

ieee = Header(
    "IEEE Honorary Membership (2015)",
    sub_header=(
        "Given to people who have rendered meritorious service to humanity in "
        "IEEE's designated fields of interest."
    ),
    alignment=Alignment.left,
    tier=HeaderTier.h4,
)

fortune_magazine = Header(
    "Businessperson of the Year by Fortune Magazine (2013)",
    sub_header=(
        "Prize received for the following companies: 'SpaceX', 'Tesla Motors' "
        "and 'Solar City'"
    ),
    alignment=Alignment.left,
    tier=HeaderTier.h4,
)

fai = Header(
    "FAI Gold Space Medal (2010)",
    sub_header=(
        "One of the highest honours in the aerospace industry, shared with "
        "prominent personalities like Neil Armstrong and John Glenn."
    ),
    alignment=Alignment.left,
    tier=HeaderTier.h4,
)

design = Header(
    "Honorary doctorate in Design from the Art College of Design",
    alignment=Alignment.left,
    tier=HeaderTier.h4,
)

aerospace = Header(
    "Honorary doctorate (DUniv) in Aerospace Engineering from the University of Surrey",
    alignment=Alignment.left,
    tier=HeaderTier.h4,
)

yale = Header(
    "Honorary doctorate of Engineering and Technology from Yale University",
    alignment=Alignment.left,
    tier=HeaderTier.h4,
)

# Create second page right column and add contents to it
second_page_right_column = Column(width=10)
second_page_right_column.add_element(education_header)
second_page_right_column.add_element(educations)
second_page_right_column.add_element(achievement_header)
second_page_right_column.add_element(ieee)
second_page_right_column.add_element(fortune_magazine)
second_page_right_column.add_element(fai)
second_page_right_column.add_element(design)
second_page_right_column.add_element(aerospace)
second_page_right_column.add_element(yale)

# Create second page grid
second_page_grid = Grid()
second_page_grid.add_layout(second_page_left_column)
second_page_grid.add_layout(second_page_right_column)

# Build the final output pdf file
report = PdfReport()
report.add_element(first_page_grid)
report.add_element(second_page_grid)
report.generate_pdf("modern_resume_tutorial.pdf")
