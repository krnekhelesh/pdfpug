#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os

from pdfpug import PdfReport
from pdfpug.common import (
    Alignment,
    HeaderStyle,
    HeaderTier,
    Color,
    SegmentSpacing,
    url,
    superscript,
)
from pdfpug.layouts import Grid, Column
from pdfpug.modules import (
    Header,
    Paragraph,
    Table,
    Image,
    UnorderedList,
    MessageBox,
    Segment,
    OrderedList,
)

# Demo message to indicate that this pdf was created using PdfPug
info_message = MessageBox(
    header="Attention!",
    body="This PDF was created using <b>PdfPug</b>. It tries to mimic the layout "
    "of Python's Wikipedia article as a way to test the capabilities and "
    "limitations of PdfPug.",
    color=Color.blue,
)

# Main title of article
main_title = Header(
    "Python (programming language)",
    sub_header="From Wikipedia, the free encyclopedia",
    alignment=Alignment.left,
)

# Define all urls used in the article here to improve code sanity
interpreted = url("https://en.wikipedia.org/wiki/Interpreted_language", "interpreted")
guido = url("https://en.wikipedia.org/wiki/Guido_van_Rossum", "Guido van Rossum")
readability = url("https://en.wikipedia.org/wiki/Code_readability", "code readability")
high_level = url(
    "https://en.wikipedia.org/wiki/High-level_programming_language", "high-level"
)
general_purpose = url(
    "https://en.wikipedia.org/wiki/General-purpose_programming_language",
    "general-purpose",
)
programming_language = url(
    "https://en.wikipedia.org/wiki/Programming_language", "programming language"
)
whitespace = url(
    "https://en.wikipedia.org/wiki/Off-side_rule", "significant whitespace"
)

# Create introduction section
intro_para = Paragraph(
    f"Python is an {interpreted}, {high_level}, {general_purpose}, "
    f"{programming_language} Created by {guido} and first released in 1991, "
    f"Python's design philosophy emphasizes {readability} with its notable use of"
    f"with its notable use of {whitespace}. Its language constructs and "
    f"object-oriented approach aim to help programmers write clear, logical code "
    f"for small and large-scale projects.{superscript('[27]')}"
    f"<br><br>Python is dynamically typed and garbage-collected. It supports multiple "
    f"programming paradigms, including procedural, object-oriented, and functional "
    f'programming. Python is often described as a "batteries included" language '
    f"due to its comprehensive standard library.{superscript('[28]')}<br><br>"
    f"Python was conceived in the late 1980s as a successor to the ABC language. "
    f"Python 2.0, released 2000, introduced features like list comprehensions and "
    f"a garbage collection system capable of collecting reference cycles. Python 3.0, "
    f"released 2008, was a major revision of the language that is not completely "
    f"backward-compatible, and much Python 2 code does not run unmodified on Python "
    f"3. Due to concern about the amount of code written for Python 2, support for "
    f"Python 2.7 (the last release in the 2.x series) was extended to 2020. Language "
    f"developer Guido van Rossum shouldered sole responsibility for the project until "
    f"July 2018 but now shares his leadership as a member of a five-person steering "
    f"council.{superscript('[29][30][31]')}<br><br>"
    f"Python interpreters are available for many operating systems. A global community "
    f"of programmers develops and maintains CPython, an open "
    f"source{superscript('[32]')} reference implementation. A non-profit organization, "
    f"the Python Software Foundation, manages and directs resources for Python and "
    f"CPython development. "
)

intro_para_column = Column(width=7)
intro_para_column.add_element(intro_para)

python_logo = Image(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), "python-logo.png")
)

intro_table = Table(
    header=["Property", "Value"],
    data=[
        [
            "Paradigm",
            "Multi-paradigm, functional, imperative, object-oriented, reflective",
        ],
        ["Designed by", "Guido van Rossum"],
        ["Developer", "Python Software Foundation"],
        ["First appeared", "1990; 29 years ago"],
        ["Stable release", "3.7.4 / 8 July 2019<br>2.7.16 / 4 March 2019"],
        ["Typing discipline", "Duck, dynamic gradual (since 3.5)"],
        ["License", "Python Software Foundation License"],
        ["Filename extensions", ".py, .pyc, .pyd, .pyo"],
    ],
)

intro_table_column = Column(width=7)
intro_table_column.add_element(python_logo)
intro_table_column.add_element(intro_table)

intro_grid = Grid()
intro_grid.add_layout(intro_para_column)
intro_grid.add_layout(intro_table_column)

# Create table of contents
contents_list = OrderedList(
    [
        "History",
        "Features and philosophy",
        {
            "Syntax and semantics": [
                "Indentation",
                "Statements and control flow",
                "Expressions",
                "Methods",
                "Typing",
                "Mathematics",
            ]
        },
        "Libraries",
        "Development environments",
        {
            "Implementations": [
                "Reference implementations",
                "Other implementations",
                "Unsupported implementations",
                "Cross-compilers to other languages",
                "Performance",
            ]
        },
        "Development",
        "Naming",
        "API documentation generators",
        "Uses",
        "Langauges influenced by Python",
        "See also",
        {"References": ["Sources"]},
        "Further reading",
        "External links",
    ]
)

contents_segment = Segment(
    [Header("Contents", tier=HeaderTier.h3), contents_list],
    spacing=SegmentSpacing.compact,
)

# Create history section
history_header = Header(
    "History", tier=HeaderTier.h2, style=HeaderStyle.dividing, alignment=Alignment.left
)

history_para = Paragraph(
    f"Python was conceived in the late 1980s{superscript('[33]')} by Guido van Rossum "
    f"at Centrum Wiskunde & Informatica (CWI) in the Netherlands as a successor to the "
    f"ABC language (itself inspired by SETL),{superscript('[34]')} capable of "
    f"exception handling and interfacing with the Amoeba operating system."
    f"{superscript('[8]')} Its implementation began in December 1989."
    f"{superscript('[35]')} Van Rossum continued as Python's lead developer until "
    f'July 12, 2018, when he announced his "permanent vacation" from his '
    f"responsibilities as Python's Benevolent Dictator For Life, a title the "
    f"Python community bestowed upon him to reflect his long-term commitment as "
    f"the project's chief decision-maker.{superscript('[36]')} In January, 2019, "
    f"active Python core developers elected Brett Cannon, Nick Coghlan, Barry Warsaw, "
    f'Carol Willing and Van Rossum to a five-member "Steering Council" to lead the '
    f'project.{superscript("[37]")} <br><br>Python 2.0 was released on 16 October '
    f"2000 with many major new features, including a cycle-detecting garbage "
    f"collector and support for Unicode.{superscript('[38]')} <br><br>Python 3.0 "
    f"was released on 3 December 2008. It was a major revision of the language that "
    f"is not completely backward-compatible.{superscript('[39]')} Many of its major "
    f"features were backported to Python 2.6.x{superscript('[40]')} and 2.7.x version "
    f"series. Releases of Python 3 include the 2to3 utility, which automates "
    f"(at least partially) the translation of Python 2 code to Python 3."
    f"{superscript('[41]')}<br><br>Python 2.7's end-of-life date was initially set "
    f"at 2015 then postponed to 2020 out of concern that a large body of existing "
    f"code could not easily be forward-ported to Python 3.{superscript('[42][43]')} "
    f"In January 2017, Google announced work on a Python 2.7 to Go transcompiler "
    f"to improve performance under concurrent workloads.{superscript('[44]')}"
)

# Create library section
library_header = Header(
    "Libraries",
    tier=HeaderTier.h2,
    style=HeaderStyle.dividing,
    alignment=Alignment.left,
)

library_para = Paragraph(
    "Python's large standard library, commonly cited as one of its greatest strengths,"
    "[97] provides tools suited to many tasks. For Internet-facing applications, "
    "many standard formats and protocols such as MIME and HTTP are supported. It "
    "includes modules for creating graphical user interfaces, connecting to relational "
    "databases, generating pseudorandom numbers, arithmetic with arbitrary precision "
    "decimals,[98] manipulating regular expressions, and unit testing."
    "<br><br>Some parts of the standard library are covered by specifications "
    "(for example, the Web Server Gateway Interface (WSGI) implementation wsgiref "
    "follows PEP 333[99]), but most modules are not. They are specified by their "
    "code, internal documentation, and test suites (if supplied). However, because "
    "most of the standard library is cross-platform Python code, only a few modules "
    "need altering or rewriting for variant implementations."
    "<br><br>As of March 2018, the Python Package Index (PyPI), the official "
    "repository for third-party Python software, contains over 130,000[100] "
    "packages with a wide range of functionality, including: "
)

library_list = UnorderedList(
    [
        "Graphical user interfaces",
        "Web frameworks",
        "Multimedia",
        "Databases",
        "Networking",
        "Test frameworks",
        "Automation",
        "Web scraping[101]",
        "Documentation",
        "System administration",
        "Scientific computing",
        "Text processing",
        "Image processing",
    ]
)

# Finally, add elements to the report and generate pdf file
report = PdfReport()
report.add_elements(
    [
        info_message,
        main_title,
        intro_grid,
        contents_segment,
        history_header,
        history_para,
        library_header,
        library_para,
        library_list,
    ]
)
report.generate_pdf("python_wiki_tutorial.pdf")
