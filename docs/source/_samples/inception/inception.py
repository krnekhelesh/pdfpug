#  MIT License
#
#  Copyright (C) 2019 Nekhelesh Ramananthan
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#  PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os

from pdfpug import PdfReport
from pdfpug.common import (
    bold,
    url,
    strike,
    superscript,
    MessageState,
    italic,
    Size,
    LabelType,
    Color,
    HeaderTier,
    Alignment,
    ImageStyle,
    TableSpacing,
    TableColumnWidth,
    TableType,
    SegmentType,
)
from pdfpug.layouts import Grid, Column
from pdfpug.modules import (
    Table,
    Row,
    Cell,
    Image,
    Header,
    Paragraph,
    MessageBox,
    UnorderedList,
    Label,
    LineBreak,
    TableOfContents,
    Segment,
    ProgressBar,
    Statistic,
)

title = Header("Inceptioned")

tutorial_tag = Label(
    text="Tutorial", color=Color.blue, label_type=LabelType.tag, size=Size.mini
)

stub_tag = Label(text="Stub", color=Color.red, label_type=LabelType.tag, size=Size.mini)

living_doc_tag = Label(
    text="Living Document", color=Color.blue, label_type=LabelType.tag, size=Size.mini
)

ambi_ending = Label(
    text="Ambiguous Ending",
    color=Color.orange,
    label_type=LabelType.tag,
    size=Size.mini,
)

left_intro = Paragraph(
    f"{bold('Inception')} as a concept has always fascinated me. I cannot exactly "
    f"pinpoint since when, but it started with trying to write code that generates "
    f"itself. This concept is called a "
    f"{url('https://en.wikipedia.org/wiki/Quine_(computing)', 'Quine')}. "
    f"That idea was always in the back of my mind and it only became more vivid after"
    f"watching the movie {url('https://en.wikipedia.org/wiki/Inception', 'Inception')}. "
    f"If you have not watched it, you definitely should. Leonardo Di Caprio is "
    f"brilliant!<br><br>"
    f"Now, if you are wondering what all this babbling has got to do with this sample "
    f"PDF? That's simple. I want to use PdfPug to generate documentation about PdfPug "
    f"elements and layouts.<br><br>"
    f"This tutorial could masquerade as a showcase for all the various capabilties "
    f"of PdfPug. Brilliant, right? No? {strike('Let me show you then!')} That's "
    f"Okay!{superscript('**')}.<br><br>"
)

right_intro = Paragraph(
    f"The {bold('source code')} and the {bold('output PDF')} is available for you to "
    f"download and explore. If you notice any discrepancies, do report a bug."
)

doc_warning = MessageBox(
    header="Attention!",
    body=(
        f"While the goal of this tutorial is to showcase all the components, it is {bold('not')} "
        f"meant to replace the official API documentation in any manner of speaking. "
        f"With that disclaimer out of the way, let's jump start this tutorial.<br><br>"
        f"{italic('Note: Btw this is a MessageBox if you did not notice ;)')}"
    ),
    state=MessageState.negative,
)

left_intro_column = Column(width=7)
left_intro_column.add_element(left_intro)
right_intro_column = Column(width=7)
right_intro_column.add_element(right_intro)
right_intro_column.add_element(doc_warning)
intro_grid = Grid()
intro_grid.add_layout(left_intro_column)
intro_grid.add_layout(right_intro_column)

real_intro_header = Header(
    "The Real Intro", tier=HeaderTier.h2, alignment=Alignment.left
)

intro_cont = Paragraph(
    "PdfPug has several components required to build a feature filled beautiful PDF "
    "file. This walkthrough will demonstrate these components in actions and the "
    "extent of customisations that these components offer. The components "
    "provided out of the box are listed below,"
)

basic_component_list = UnorderedList(
    [
        {
            f"{bold('Basic Components')}": [
                "Paragraph",
                "Header",
                "Table",
                "List",
                "Image",
            ]
        }
    ],
    size=Size.medium,
)

extra_component_list = UnorderedList(
    [
        {
            f"{bold('Extras')}": [
                "Label",
                "MessageBox",
                "ProgressBar",
                "Segment",
                "Statistic",
                "Table of Contents",
            ]
        }
    ],
    size=Size.medium,
)

left_component_column = Column(width=4)
left_component_column.add_element(basic_component_list)
right_component_column = Column(width=4)
right_component_column.add_element(extra_component_list)
component_grid = Grid()
component_grid.add_layout(left_component_column)
component_grid.add_layout(right_component_column)

component_brief = Paragraph(
    f"All PdfPug components are essentially classes that can be instantiated with "
    f"arguments to modify their behavior and appearance. Before generating the PDF "
    f"file, the instantiated components will need to be added to the "
    f"{italic('PdfReport')} object using its {italic('add_elements()')} method "
    f"for it to be included in the PDF file."
)

tabular_digest = Paragraph(
    f"Keeping that in mind, let's dive back to our preface topic which was about "
    f"Inception. Let's try to present the cast of our favorite movie Inception in "
    f"an easy to read tabular form as shown below. The style (spacing, border style) "
    f"complexity of content (simple text to images, headers) etc are all supported "
    f"by the table.<br><br>"
    f"Customizing tables involves becoming familiar with 2 concepts which are "
    f"{bold('Rows')} and {bold('Cells')}. Refer to the official API documentation "
    f"for more details on how to use these."
)

root_dir = os.path.dirname(os.path.realpath(__file__))

inception_cast = Table(
    data=[
        Row(
            [
                Cell(
                    Image(
                        os.path.join(root_dir, "caprio.jpg"), style=ImageStyle.circular
                    )
                ),
                "Leonardo DiCaprio",
                "Cobb",
            ]
        ),
        Row(
            [
                Cell(
                    Image(
                        os.path.join(root_dir, "joseph.jpg"), style=ImageStyle.circular
                    )
                ),
                "Joseph Gordon-Levitt",
                "Arthur",
            ]
        ),
        Row(
            [
                Cell(
                    Image(
                        os.path.join(root_dir, "ellen.jpg"), style=ImageStyle.circular
                    )
                ),
                "Ellen Page",
                "Ariadne",
            ]
        ),
        Row(
            [
                Cell(
                    Image(os.path.join(root_dir, "tom.jpg"), style=ImageStyle.circular)
                ),
                "Tom Hardy",
                "Eames",
            ]
        ),
    ],
    table_type=TableType.simple,
    spacing=TableSpacing.compact,
    column_width_rule=TableColumnWidth.minimum,
)

synopsis = Segment(
    [
        Header("Storyline", tier=HeaderTier.h3),
        Paragraph(
            f"Dom Cobb is a skilled thief, the absolute best in the dangerous art of "
            f"extraction, stealing valuable secrets from deep within the subconscious "
            f"during the dream state, when the mind is the most vulnerable. Cobb's "
            f"rare ability has made him a coveted player in this treacherous new "
            f"world of corporate espionage, but it has also made him an international "
            f"fugitive and cost him everything he has ever loved."
        ),
    ],
    segment_type=SegmentType.piled,
)

cast_column = Column(width=6)
cast_column.add_element(inception_cast)
storyline_column = Column(width=8)
storyline_column.add_element(synopsis)
table_segment_grid = Grid()
table_segment_grid.add_layout(cast_column)
table_segment_grid.add_layout(storyline_column)

conclusion = Header("Conclusion", tier=HeaderTier.h2, alignment=Alignment.left)

finish = Paragraph(
    "If you have come this far, you have already seen a majority of the components "
    "offered by PdfPug in action. Hope this gives you a taste of what PdfPug offers. "
    "If you have any suggestions or notice any bug, do give us a shout out on the "
    "project Gitlab page."
)

coverage = ProgressBar(95, title="PdfPuf Component Discovery", color=Color.blue)

meta_header = Header("Meta", tier=HeaderTier.h2, alignment=Alignment.left)

meta_info = Paragraph(
    f"Did you know, this PDF's properties will show you a customized meta information? "
    f"Check the file properties. It should show an {italic('author, title, description')} "
    f"and {italic('keywords')} all of which were programmatically set. Its not hard. "
    f"It basically involves calling the {italic('set_meta_information()')} method "
    f"provided by the {italic('PdfReport')} class. Let's go ahead and add a table "
    f"about our favorite movie Inception"
)

trivia_header = Header("Trivia", tier=HeaderTier.h2, alignment=Alignment.left)

trivia = Paragraph(
    f"Almost forgot about the trivia section. Here's to the movie trivia copied from "
    f"Imdb,"
)

run_time = Statistic(label="DVD Run Time", value=8888)
downloads = Statistic(label="Million BitTorrent Downloads", value=9.72)

report = PdfReport()
report.set_meta_information(
    title="PdfPug Inception",
    description="Learn about PdfPug's components in a different way!",
    authors=["Nekhelesh Ramananthan"],
    keywords=["Tutorial", "Stub", "Living Document"],
)
report.add_elements(
    [
        title,
        tutorial_tag,
        living_doc_tag,
        ambi_ending,
        stub_tag,
        LineBreak(lines_count=2),
        intro_grid,
        TableOfContents(),
        real_intro_header,
        intro_cont,
        component_grid,
        LineBreak(),
        component_brief,
        tabular_digest,
        table_segment_grid,
        trivia_header,
        trivia,
        run_time,
        downloads,
        meta_header,
        meta_info,
        conclusion,
        finish,
        coverage,
    ]
)
report.generate_pdf("inception.pdf")
