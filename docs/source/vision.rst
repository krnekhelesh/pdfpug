Vision
======

If you are wondering about the motivation behind creating yet another Python
library to create PDF files? The answer is simple. The Python ecosystem does
not have a library that is **simple** enough to use while providing the means
to create **rich beautiful professional** PDFs. Either they are simple yet
lacking certain features or they are fully featured but difficult to use.

That's great. **Is PdfPug the solution to the above problem?** Well, it is
**intended** to be. At the moment, it is still in its infancy to match up to
that expectation. But that is the vision that drives the development efforts
of PdfPug. Also, balancing features with ease of use is a difficult balance
to achieve, but something that is worth trying.

How do we measure success? Just about anyone should be able to create
professional looking PDFs in a live coding session. It should not be
intimidating.

If this is something that excites you, and you are interested in helping make
that a reality, then please do check out the :doc:`contributing`.
