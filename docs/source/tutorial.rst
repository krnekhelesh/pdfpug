Tutorial
========

This section strives to introduce PdfPug by means of examples. The examples
vary in difficult from simple to complex PDF files. By going through these
examples, one can get a good idea of the capabilities and ease of use of
PdfPug. Each example will provide a detailed explanation of how we arrived
at the final output. The links to the full source code and output PDF file
will be provided for each example. You are encouraged to download them and
explore them. Tweak them. Improve them.

.. note::
    The examples below are also used by the library author to dogfood the
    library APIs and check if the usage pattern is simple and understandable.

Prerequisites
-------------

It is recommended to set up a clean environment before starting the examples.
Let's quickly do that before diving into the examples.

1. Create a new folder
2. Inside the newly created folder, set up a `Python Virtual Environment
   <https://docs.python.org/3/tutorial/venv.html>`_
3. Activate Environment
4. Pip install PdfPug

The commands for Linux are provided below for reference,

.. code-block:: bash

    mkdir pdfpug-examples && cd pdfpug-examples
    python -m venv venv
    source venv/bin/activate
    pip install pdfpug

.. toctree::
   :maxdepth: 1
   :titlesonly:
   :hidden:

   _tutorials/pythonwiki
   _tutorials/modernresume
   _tutorials/inception
