Changelog
=========

0.5 (21-19-2019)
----------------

- Add :py:class:`~pdfpug.modules.Statistic` element
- Add :py:class:`~pdfpug.modules.Label` element
- Fixed :py:class:`~pdfpug.modules.TableOfContents` not showing page numbers
- Add support for customizing the PDF pages like showing/hiding page numbers
  at the bottom of every page, size and orientation
- Add inception tutorial to document PdfPug using PdfPug
- Integrate deepsource.io for static code analysis

0.4 (18-09-2019)
----------------

- Support setting meta information of document like title, description,
  authors and keywords
- Support customizing column widths of :py:class:`~pdfpug.modules.Table`
- Support headless :py:class:`~pdfpug.modules.Table`
- Add :py:class:`~pdfpug.modules.ProgressBar` element
- Add :py:class:`~pdfpug.modules.TableOfContents` element
- Add text format helpers for :py:func:`~pdfpug.common.bold`,
  :py:func:`~pdfpug.common.italic`. :py:func:`~pdfpug.common.underline`,
  :py:func:`~pdfpug.common.strike`, :py:func:`~pdfpug.common.superscript`,
  :py:func:`~pdfpug.common.subscript` and :py:func:`~pdfpug.common.url`
- Add support for customizing the appearance of reports using predefined
  themes. `Mood Swing` is the first theme to be included in
  :py:class:`~pdfpug.common.Theme`
- Documented :py:class:`~pdfpug.common.TableType` with images for better
  clarity
- Add tutorials to demonstrate usage of APIs and showcase sample PDF files
  created using PdfPug

0.3 (05-09-2019)
----------------

- Add basic layout system :py:class:`~pdfpug.layouts.Grid`,
  :py:class:`~pdfpug.layouts.Row` and :py:class:`~pdfpug.layouts.Column`
- Add support for :py:class:`~pdfpug.modules.Header` captions (subheader)
- Fixed :py:class:`~pdfpug.modules.Table` not being inserted correctly
  into other elements like :py:class:`~pdfpug.modules.Segment`
- Improved documentation of :py:class:`~pdfpug.modules.Table`
- Moved all elements to `pdfpug.modules` namespace to improve clarity
  and avoid naming conflicts
- Style and position enums can now only be accessed through `pdfpug.common`
  to improve clarity

0.2 (26-08-2019)
----------------

- Add new elements :py:class:`~pdfpug.modules.Image`,
  :py:class:`~pdfpug.modules.PageBreak`, :py:class:`~pdfpug.modules.Segment`
  and :py:class:`~pdfpug.modules.LineBreak`
- Add ``h4`` and ``h5`` header tiers
- Setup pytest, Gitlab CI/CD and pre-commit
- Removed unnecessary dependencies like pandas, markdown etc

0.1 (20-08-2019)
----------------

- Add new elements :py:class:`~pdfpug.modules.Header`,
  :py:class:`~pdfpug.modules.Table`, :py:class:`~pdfpug.modules.OrderedList`,
  :py:class:`~pdfpug.modules.UnorderedList`, and
  :py:class:`~pdfpug.modules.Paragraph` elements
- Setup documentation system using Sphinx
- Project init
