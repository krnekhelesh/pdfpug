API Documentation
=================

Modules are the building blocks of a PDF report. PdfPug provides several
modules like :py:class:`~pdfpug.modules.Header`,
:py:class:`~pdfpug.modules.OrderedList` that can be used to put together
a PDF report. Modules can also take other modules as inputs.

.. toctree::
   :maxdepth: 1
   :titlesonly:

   _modules/pdfreport
   _modules/header
   _modules/paragraph
   _modules/list
   _modules/label
   _modules/table
   _modules/image
   _modules/segment
   _modules/linebreak
   _modules/messagebox
   _modules/pagebreak
   _modules/progressbar
   _modules/statistic
   _modules/tableofcontents
   _common/enums
   _common/helpers
   _layouts/layouts
   _common/theme
