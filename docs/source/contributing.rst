Contributing Guide
==================

Looking to contribute? Thank you very much! **All contributions are welcome**
even if they are as small as fixing a typo in the documentation. Just the fact
that you are reading this document to learn how to contribute is a big deal to
the library author!

A bit of of a confession. This document is a stub with limited exposure to the
outside world. If you hit a road block anywhere or find the instructions to be
outdated or incorrect, do report it as a bug
`here <https://gitlab.com/krnekhelesh/pdfpug/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=>`_
or even better create a pull request with the fix.

Let's get started.

The first step towards contributing either code or documentation changes involves
grabbing the library's source code. You can do that by forking the
`Gitlab repo <https://gitlab.com/krnekhelesh/pdfpug>`_.

.. note::
    When contributing new features, it is best to discuss the solution before
    diving into the implementation. The simplicity and ease of use of the public
    APIs are important goals in achieving the :doc:`vision </vision>` of PdfPug.
    This is something that can be verified only by discussing it with others.

Code Contributions
------------------

PdfPug at its core functions by providing an easy to use API for users to create
.pug files which is parsed by `PyPugJS <https://github.com/akubera/pypugjs>`_ and
then converted into a PDF using `WeasyPrint <http://weasyprint.org/>`_. All the
modules provided by PdfPug essentially create mini pug snippets that are then
compiled together to create the final PDF.

1. Start by installing the development dependencies using `pip install -r
   requirements.txt`.

2. Run the tests to confirm if they all pass on your system.

3. Write tests that demonstrate your bug or feature. Ensure that they fail
   (Test Driven Development - TDD).

4. Make your change.

5. Run the entire test suite again, confirming that all tests pass.

6. Send a pull request to the master branch of the
   `Gitlab repo <https://gitlab.com/krnekhelesh/pdfpug>`_.

Documentation Contributions
---------------------------

The documentation lives in the `docs` directory. They are written in
`reStructuredText <http://docutils.sourceforge.net/rst.html>`_, and use
`Sphinx <http://sphinx-doc.org/index.html>`_ to generate the full suite of
documentation.

1. Start by installing the development dependencies using `pip install -r
   requirements.txt`.

2. Navigate to the `docs` directory

3. Make changes to the documentation.

4. Generate the documentation by running `make html`

5. Open the `index.html` that is generated in the `docs/build` folder. This
   should show you the updated documentation with your changes.
