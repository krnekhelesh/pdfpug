Welcome to PdfPug's documentation!
==================================

**PdfPug** is a tool that makes it **easy** to create **rich beautiful PDFs**
from **scratch**. It provides **simple APIs** that allow for quick creation
of PDF files without any fuss. Read more about the vision that drives the
development efforts of this library :doc:`here </vision>`.

PdfPug consists of small building blocks like
:py:class:`~pdfpug.modules.Table`, :py:class:`~pdfpug.modules.Header`,
:py:class:`~pdfpug.modules.OrderedList` etc. and the ability to customise
these component to suit different use cases.

.. note::
    The library is still very new and as a result the APIs can be assumed to not
    be stable. Do not use this library in production. Any bugs found can be
    reported at the project's `Gitlab repo <https://gitlab.com/krnekhelesh/pdfpug>`_.

Here is a small example to create a basic PDF that contains a header and a
paragraph,

.. code-block:: python

   from pdfpug.modules import Header, Paragraph
   from pdfpug import PdfReport

   # Create modules
   intro_header = Header("Introduction to PdfPug")
   para = Paragraph(
       "Lorem Ipsum is <b>simply</b> <u>dummy</u> text of the printing and typesetting "
       "industry. Lorem Ipsum has been the industry's standard dummy text "
       "ever since the 1500s, when an unknown printer took a galley of type"
       " and scrambled it to make a type specimen book. It has survived not "
       "only five centuries, but also the leap into electronic typesetting, "
       "remaining essentially unchanged. It was popularised in the 1960s with "
       "the release of Letraset sheets containing Lorem Ipsum passages, and "
       "more recently with desktop publishing software like Aldus PageMaker "
       "including versions of Lorem Ipsum."
   )

   # Create a PdfReport object
   report = PdfReport()

   # Add modules to report
   report.add_element(intro_header)
   report.add_element(para)

   # Generate PDF Report
   report.generate_pdf("test-report.pdf")

Licensing
---------

PdfPug uses the **MIT** license and as such is open source and welcomes contribution.
The license file can be found in the project's
`Gitlab repo <https://gitlab.com/krnekhelesh/pdfpug>`_.

.. toctree::
   :hidden:
   :maxdepth: 1
   :titlesonly:

   vision
   tutorial
   modules
   changelog
   contributing
