Text Format Helpers
===================

The text formatter functions help set various styles to text like making it
bold, underline, strikethrough, superscript etc. They can be used within any
of PdfPug's modules to further customize them to suit different use cases.

For example,

.. code-block:: python

    from pdfpug.modules import Paragraph, Header

    # Italicized header
    header = Header(italic("PdfPug"))

    # Paragraph with a URL
    para = Paragraph(f"{url('https://pypi.org/project/pdfpug/', 'PdfPug PyPi Page')}")

.. autofunction:: pdfpug.common.url
.. autofunction:: pdfpug.common.bold
.. autofunction:: pdfpug.common.italic
.. autofunction:: pdfpug.common.underline
.. autofunction:: pdfpug.common.strike
.. autofunction:: pdfpug.common.superscript
.. autofunction:: pdfpug.common.subscript
