Enums
=====

.. autoclass:: pdfpug.common.Alignment
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.Color
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.Size
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.Orientation
    :members:
    :member-order: bysource

.. autoclass:: pdfpug.common.State
    :members:
    :member-order: bysource
