Theme
=====

PdfPug comes with a set of curated themes that work beautiful with all
PdfPug elements like :py:class:`~pdfpug.modules.Header` and others
defined in the :doc:`../modules` section.

Using these themes is as simple as passing it as an argument to the
:py:class:`~pdfpug.PdfReport` class as shown below,

>>> from pdfpug import PdfReport
>>> from pdfpug.common import Theme
>>> report = PdfReport(theme=Theme.mood_swing)
>>> report.generate_pdf("pdfpug.pdf")

.. warning::
    Themes are an **experimental** feature that is in a state of flux.
    Expect frequent API breakage!

.. autoclass:: pdfpug.common.Theme
    :members:
    :member-order: bysource
